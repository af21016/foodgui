import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

public class ex_1 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton soba207yenButton;
    private JButton curryAndRice524yenButton;
    private JButton sushiButton;
    private JButton checkOutButton;
    private JTextArea orderedItemList;
    private JLabel totalYen;
    private JLabel totalPrice;
    private JLabel explanationThings;

    public ex_1() {
        totalPrice.setText("0");
        tempuraButton.setIcon(new javax.swing.ImageIcon(".\\src\\tempura.png"));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//天ぷらボタンを押したとき
                //button.setIcon(new ImageIcon(this.getClass().getResource(path)));
                order("Tempura", 700);
            }
        });
        ramenButton.setIcon(new javax.swing.ImageIcon(".\\src\\ramen.png"));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//ラーメンボタンを押したとき
                order("Ramen", 650);
            }
        });
        udonButton.setIcon(new javax.swing.ImageIcon(".\\src\\udon.png"));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//うどんボタンを押したとき
                order("Udon", 590);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//チェックアウトボタンを押したとき
                checkout();
            }
        });
        curryAndRice524yenButton.setIcon(new javax.swing.ImageIcon(".\\src\\curry_and_rice.png"));
        curryAndRice524yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//カレーボタンを押したとき
                order("Curry and Rice", 620);
            }
        });
        soba207yenButton.setIcon(new javax.swing.ImageIcon(".\\src\\soba.png"));
        soba207yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//そばボタンを押したとき
                order("Soba", 630);
            }
        });
        sushiButton.setIcon(new javax.swing.ImageIcon(".\\src\\sushi.png"));
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){//寿司ボタンを押したとき
                order("Sushi", 720);
            }
        });
    }

    public void order(String food, int priceRange){//注文時の動作
        String[] sizes = {"Normal", "Large"};
        int foodConfirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);//注文するかの確認
        int size = 0;
        if(foodConfirmation == 0){//注文するとき
            size = JOptionPane.showOptionDialog(null, "Chose the Size", "Size Select",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, sizes, sizes[1]);//サイズの選択
            int decision = JOptionPane.showConfirmDialog(null,
                    food + "(" + sizes[size] + ")" + " Selected.",
                    "Check select things.",JOptionPane.YES_NO_OPTION);//サイズ選択
            if(decision == 0){
                int[] foodSize = {0,100};//Normalなら+0円、Largeなら+100円
                int price = priceRange;
                int eatHereConfirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to eat " + food + " here?",
                        "Eat Here Confirmation", JOptionPane.YES_NO_OPTION);//イートインかの確認
                if(eatHereConfirmation == 0){//イートインなら
                    price = priceRange + foodSize[size];
                    JOptionPane.showMessageDialog(null,
                            "Order for " + food + " ("+ sizes[size] + ") (eat here) received.");//注文を受け取ったことを通知
                    String currentText = orderedItemList.getText();
                    orderedItemList.setText(currentText + food + " (" + sizes[size] +") (eat here) "+ price + " yen\n");
                }else if(eatHereConfirmation == 1){//テイクアウトなら
                    price = (priceRange + foodSize[size]) - 50;
                    JOptionPane.showMessageDialog(null,
                            "Order for " + food + " (" + sizes[size] + ") (takeout) received.");
                    String currentText = orderedItemList.getText();
                    orderedItemList.setText(currentText + food + " (" + sizes[size] + ") (takeout) "+ price + " yen\n");
                }
                totalPrice.setText(String.valueOf((calculateTotalPrice(price))));
            }
        }
    }

    public int calculateTotalPrice(int total){
        int TotalPrice = Integer.parseInt(totalPrice.getText());
        return TotalPrice + total;
    }

    public void checkout(){
        int checkoutConfirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to Checkout?",
                "Checkout Confirmation", JOptionPane.YES_NO_OPTION);//チェックアウトをするかの確認
        if(checkoutConfirmation == 0){//チェックアウトをするときの動作
            JOptionPane.showMessageDialog(null,
                    "Thank you. The total price is " + calculateTotalPrice(0) + " yen.");
            orderedItemList.setText("");//注文済みリストを空にする
            totalPrice.setText("0");//合計金額を0にする
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex_1");
        frame.setContentPane(new ex_1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
